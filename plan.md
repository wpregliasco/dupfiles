# Files duplicados 

La idea es proveer dos árboles de directorios e identificar los archivos 
de un árbol que están en el otro. 

Me gustaría proveer una solución independiente del sistema operativo.

## Escaneo de árboles

## Asignación de orden

## Identificación de iguales

Empezamos con estrategias en linux
https://www.makeuseof.com/best-tools-find-and-remove-duplicate-files-linux/

* fslint (GUI+framework)
* fdupes (md5sum)
* rdfind (checksums)
* dupeguru (fuzzy matching: images, audio...)

[Locality sensitive hashing](https://towardsdatascience.com/understanding-locality-sensitive-hashing-49f6d1f6134)
[Structural Similarity Index & Mean Squared Error](https://pyimagesearch.com/2014/09/15/python-compare-two-images/)
(skimage)

[Estrategia CRC _then_ hash](https://stackoverflow.com/questions/54000349/hashing-1000-image-files-quick-as-possible-2000x2000-plus-resolution-python)

[Perceptual image hash](https://content-blockchain.org/research/testing-different-image-hash-functions/)

[PiPy ImageHash](https://pypi.org/project/ImageHash/)

[Python Fast CRC](https://fastcrc.readthedocs.io/en/latest/)

